#!/usr/bin/python3

import sys
import gc
from enum import Enum
from tkinter import *
from tkinter import filedialog
from PIL import Image, ImageTk
import json
import numpy

class ProcState(Enum):
    file_load     = 0
    click         = 1
    insert        = 2


class GUI:
    def __init__(self, root, path):
        self.status_bar = StringVar();
        bar = Label(root, textvariable=self.status_bar, bd=1, relief=SUNKEN, anchor=W)
        bar.pack(side=BOTTOM, fill="x")

        frame = Frame(root)
        frame.grid_rowconfigure(0, weight=1)
        frame.grid_columnconfigure(0, weight=1)
        frame.pack(fill="both", expand=True)

        self.canvas = Canvas(frame)
        self.canvas.grid(row=0, column=0, sticky=N+S+E+W)
        self.canvas.pack(fill="both", expand=True)

        self.canvas.bind("<Button-1>", self.select)
        self.canvas.bind("<ButtonRelease-1>", self.drop_point)
        self.canvas.bind("<Button 3>",self.grab_image)
        self.canvas.bind("<ButtonRelease-3>", self.drop_image)
        self.canvas.bind("<MouseWheel>",self.zoom) #windows
        self.canvas.bind("<Button-4>", self.zoom) #linux
        self.canvas.bind("<Button-5>", self.zoom) #linux
        self.canvas.bind("<Motion>",self.motion)
        self.canvas.bind("<Configure>",self.resize)
        root.bind("<Key>", self.key_pressed)
        root.bind("<Return>", self.enter_pressed)
        root.bind("<BackSpace>", self.backspace_pressed)
        root.bind("<Escape>", self.esc_pressed)
        root.bind("<Delete>", self.del_pressed)

        self.state = ProcState.file_load
        if path != '':
            self.load_image(path)
        else:
            self.find_image_file()
        self.update_bar()

    def load_image(self, path):
        try:
            self.orig_img = Image.open(path)

            self.offset_x, self.offset_y = 0, 0
            self.scale = 1.0
            self.img = None
            self.img_id = None

            self.point_color="#00ff00"
            # logic state
            self.grab_x, self.grab_y = 0, 0
            self.old_point_grab_x, self.old_point_grab_y = 0, 0
            self.image_grabbed = False
            self.point_grabbed = -1
            self.state = ProcState.click
            self.coord_x_tmp = ""
            self.coord_string = ""

            self.cct_filename = path + ".cct"
            # real data
            try:
                self.read_file()
            except FileNotFoundError:
                self.image_points=[]
                self.coord_points=[]
                self.logarithmic_x = True

            # draw the initial image at 1x scale
            self.redraw_now()
            self.update_bar()
        except FileNotFoundError:
            pass

    #-------------------------------------------functions---------------------------------------
    def print_state(self):
        print('------------draw--------------')
        print('offset_x,y {} {}'.format(self.offset_x, self.offset_y))
        print('scale {}'.format(self.scale))
        print('point_color {}'.format(self.point_color))
        print('------------state-------------')
        print('grab_x,y {} {}'.format(self.grab_x, self.grab_y))
        print('old_point_grab_x,y {} {}'.format(self.old_point_grab_x, self.old_point_grab_y))
        print('old_point_grab_x,y {} {}'.format(self.old_point_grab_x, self.old_point_grab_y))
        print('image_grabbed {}'.format(self.image_grabbed))
        print('point_grabbed {}'.format(self.point_grabbed))
        print('state {}'.format(self.state))
        print('coord_x_tmp {}'.format(self.coord_x_tmp))
        print('coord_string {}'.format(self.coord_string))
        print('------------data--------------')
        print('image_points {}'.format(self.image_points))
        print('coord_points {}'.format(self.coord_points))
        print('logarithmic_x {}'.format(self.logarithmic_x))

    def read_file(self):
        cct_file = open(self.cct_filename, "r")
        self.image_points = json.loads(cct_file.readline())
        self.coord_points = json.loads(cct_file.readline())
        self.logarithmic_x = json.loads(cct_file.readline())

    def write_file(self):
        cct_file = open(self.cct_filename, "w")
        cct_file.write(json.dumps(self.image_points) + '\n')
        cct_file.write(json.dumps(self.coord_points) + '\n')
        cct_file.write(json.dumps(self.logarithmic_x) + '\n')

    def find_image_file(self):
        path = filedialog.askopenfilename(initialdir = ".",title = "Select image file",filetypes = ([("image files",".jpg .png .bmp .tiff")]))
        if path != () and path != '':
            self.load_image(path)

    def image_coords(self, window_x, window_y):
        return window_x / self.scale + self.offset_x, window_y / self.scale + self.offset_y

    def window_coords(self, image_x, image_y):
        return (image_x - self.offset_x) * self.scale, (image_y - self.offset_y) * self.scale

    def find_point_nearby(self, image_x, image_y):
        for i, point in enumerate(self.image_points):
            cur_x, cur_y = point
            if image_x > cur_x -7 and \
               image_x < cur_x +7 and \
               image_y > cur_y -7 and \
               image_y < cur_y +7:
                self.old_point_grab_x, self.old_point_grab_y = cur_x, cur_y
                return i
        return -1

    def set_offset(self, x, y):
        i_width, i_height = self.orig_img.size
        global root
        w_width, w_height = root.winfo_width(), root.winfo_height()

        self.offset_x = min(max (-w_width,x),i_width)
        self.offset_y = min(max(-w_height,y),i_height)

    def redraw(self, x, y):
        if self.img_id:
            self.canvas.delete("all")

        i_width, i_height = self.orig_img.size
        global root
        w_width, w_height = root.winfo_width(), root.winfo_height()

        self.offset_x = min(max (-w_width,x),i_width)
        self.offset_y = min(max(-w_height,y),i_height)

        # calculate crop rect
        cw, ch = w_width / self.scale, w_height / self.scale

        # crop it
        tmp = self.orig_img.crop((self.offset_x, self.offset_y, self.offset_x + int(cw), self.offset_y + int(ch)))
        size = int(cw * self.scale), int(ch * self.scale)
        # draw
        self.img = ImageTk.PhotoImage(tmp.resize(size))
        self.img_id = self.canvas.create_image(w_width / 2, w_height / 2, image=self.img)
        gc.collect()

        for cur_x, cur_y in self.image_points[:3]:
            cur_x, cur_y = self.window_coords(cur_x, cur_y)
            self.canvas.create_line(cur_x, cur_y -10, cur_x, cur_y +10, fill=self.point_color)
            self.canvas.create_line(cur_x -10, cur_y, cur_x +10, cur_y, fill=self.point_color)

        for cur_x, cur_y in self.image_points[3:]:
            cur_x, cur_y = self.window_coords(cur_x, cur_y)
            self.canvas.create_line(cur_x -10, cur_y -10, cur_x +10, cur_y +10, fill=self.point_color)
            self.canvas.create_line(cur_x -10, cur_y +10, cur_x +10, cur_y -10, fill=self.point_color)

    def update_bar(self):
        text = ''
        if self.state == ProcState.file_load:
            text = "Press 'o' to open image file"

        elif self.state == ProcState.click:
            point_ind = -1;
            if self.point_grabbed < 0:
                text = "Choose"
                point_ind = len(self.image_points)
            else:
                text = "Edit"
                point_ind = self.point_grabbed
            if point_ind < 3:
                text += " coord"
            else:
                point_ind -= 3
            text += " point " + str(point_ind +1)
            if self.point_grabbed >=0 and self.point_grabbed < 3:
                real_x, real_y = self.coord_points[point_ind]
                text += " { " + str(real_x) + " ; " + str(real_y) + " }"

        elif self.state == ProcState.insert:
            point_ind = -1;
            if self.point_grabbed < 0:
                text = "Set"
                point_ind = len(self.coord_points)
            else:
                text = "Edit"
                point_ind = self.point_grabbed
            text = "real coords for point " + str(point_ind +1)
            if self.coord_x_tmp == "":
                text += " { " + self.coord_string + " ; }"
            else:
                text += " { " + self.coord_x_tmp + " ; " + self.coord_string + " }"

        self.status_bar.set(text)

    #------------------------------------------handlers-----------------------------------------
    #mouse events
    def grab_image(self,event):
        if self.state != ProcState.file_load:
            self.image_grabbed = True
            self.grab_x, self.grab_y = self.image_coords(event.x, event.y)

    def drop_image(self,event):
        if self.state != ProcState.file_load:
            self.image_grabbed = False

    def zoom(self,event):
        if self.state != ProcState.file_load:
            old_scale = self.scale
            old_x, old_y = self.image_coords(event.x, event.y)
            if event.num == 5 or event.delta == -120: self.scale = max(0.5, self.scale / 1.1)
            if event.num == 4 or event.delta ==  120: self.scale = min(8, self.scale * 1.1)
            if self.scale != old_scale:
                self.redraw(old_x - event.x / self.scale, old_y - event.y / self.scale)

    def redraw_now(self):
        if self.state != ProcState.file_load:
            self.redraw(self.offset_x, self.offset_y)

    def resize(self,event):
        global root
        root.update()
        self.redraw_now()

    def motion(self,event):
        if self.state != ProcState.file_load:
            if self.state == ProcState.click and self.point_grabbed >= 0:
                self.image_points[self.point_grabbed] = self.image_coords(event.x, event.y)
                if not self.image_grabbed:
                    self.redraw_now()
            if self.image_grabbed:
                self.redraw(self.grab_x - event.x / self.scale, self.grab_y - event.y / self.scale)

    def select(self,event):
        if self.state == ProcState.click and self.point_grabbed < 0:
            x, y = self.image_coords(event.x, event.y)
            self.point_grabbed = self.find_point_nearby(x, y)
            if self.point_grabbed < 0: # point still not grabbed
                self.image_points.append([x, y])
                if len(self.coord_points) < 3:
                    self.state = ProcState.insert
                else:
                    self.write_file()
                self.redraw_now()
            self.update_bar()

    def drop_point(self,event):
        if self.state == ProcState.click and self.point_grabbed >= 0:
            self.point_grabbed = -1
            self.update_bar()
            self.write_file()

    #keyboard events
    def del_pressed(self,event):
        if self.state == ProcState.click and self.point_grabbed >= 0:
            del self.image_points[self.point_grabbed]
            if self.point_grabbed < 3:
                del self.coord_points[self.point_grabbed]
                self.image_points = self.image_points[0:2]
            self.point_grabbed = -1
            self.redraw_now()
            self.update_bar()
            self.write_file()

    def esc_pressed(self,event):
        if self.state == ProcState.insert:
            self.state = ProcState.click
            self.coord_string = ""
            self.coord_x_tmp = ""
            if self.point_grabbed < 0:
                del self.image_points[-1]
            self.redraw_now()
            self.update_bar()
        if self.state == ProcState.click and self.point_grabbed >= 0:
            self.image_points[self.point_grabbed] = [self.old_point_grab_x, self.old_point_grab_y]
            self.old_point_grab_x, self.old_point_grab_y = 0, 0
            self.point_grabbed = -1
            self.redraw_now()
            self.update_bar()


    def enter_pressed(self,event):
        if self.state == ProcState.insert and bool(set('0123456789').intersection(self.coord_string)): #current string contains digits
            if self.coord_x_tmp == "":
                self.coord_x_tmp = self.coord_string
            else:
                if self.point_grabbed < 0:
                    self.coord_points.append([float(self.coord_x_tmp), float(self.coord_string)])
                else:
                    self.coord_points[self.point_grabbed] = [float(self.coord_x_tmp), float(self.coord_string)]
                    self.point_grabbed = -1
                self.coord_x_tmp = ""
                self.state = ProcState.click
                self.write_file()
            self.coord_string = ""
            self.update_bar()

    def backspace_pressed(self,event):
        if self.state == ProcState.insert:
            if self.coord_string != "":
                self.coord_string = self.coord_string[:-1]
            elif self.coord_x_tmp != "":
                self.coord_string = self.coord_x_tmp
                self.coord_x_tmp = ""
            self.update_bar()
        if self.state == ProcState.click and self.point_grabbed >= 0 and self.point_grabbed < 3:
            self.state = ProcState.insert
            x, y = self.coord_points[self.point_grabbed]
            self.coord_string = str(y)
            self.coord_x_tmp = str(x)

    def key_pressed(self,event):
        if self.state != ProcState.insert and event.char == 'o':
            self.find_image_file()
        elif self.state != ProcState.file_load:
            if event.char == 'l':
                self.logarithmic_x = not self.logarithmic_x
            elif event.char == 'p':
                self.print_state()
            elif self.state == ProcState.insert:
                if event.char.isnumeric():
                    self.coord_string += event.char
                elif event.char == '.':
                    if '.' not in self.coord_string:
                        self.coord_string += '.'
                elif event.char == '-':
                    if self.coord_string == "":
                        self.coord_string += '-'
                self.update_bar()



root = Tk()
root.geometry("500x500")
path = ''
if len(sys.argv) > 1:
    path = sys.argv[1]
GUI(root, path)
root.mainloop()

