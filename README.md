# CCT-image-processor

Tool for digitization of Continuous Cooling Transformation diagrams to get point values out of it
with minimal interface to make it faster

# How to use

1. start the program  
2. open an image file ('o')  
3. to pan the image:  
    3.1 press right mouse button  
    3.2 drag image with right mouse button pressed  
4. zoom in and out with mouse scroll  
5. select 3 coordinate points (should't be in one line):  
    5.1 left mouse button click  
    5.2 write real argument value (x)  
        5.2.1 allowed symbols (0-9 . -)  
        5.2.2 backspace can be used for correction  
        5.2.3 press enter when value is entered  
    5.3 write real function value (y)  
        5.3.1 same as 5.2.*  
        5.3.2 can get back to real argument value editing with backspace if value (y) is empty  
    5.4 cancel coordinate point creation with esc  
6. add points on the diagram (left mouse button click)  
7. edit point position:  
    7.1 select a point by pressing left mouse button nearby point  
    7.2 drag point with left mouse button pressed  
    7.3 delete point by pressing delete when point is being dragged (WARNING! if coordinate point was deleted all non-coordinate points will be deleted as well)  
    7.4 cancel point drag by pressing esc  
    7.5 change real function and argument values of coordinate point by pressing backspace when point is being dragged  
        7.5.1 same as 5.3.*  
        7.5.2 cancel point real function and argument values change by pressing esc  

# Keys help

o - open image file  
l - toggle logarithmic x axis  
g - toggle grid for real argument-value space  
p - print state of the program to standard output  


## Contributing.

Development is discontinued.

## License.

[MIT](https://choosealicense.com/licenses/mit/)

